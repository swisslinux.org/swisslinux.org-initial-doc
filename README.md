---

Documentation pour le prochain site web de Swisslinux
=====================================================

Ce projet contient la documentation initialement écrite pour le
nouveau site web de Swisslinux.org.

Le dossier `doc` contient la dite documentation et est structuré comme
suit:
- `Analyse`: Contient l'analyse des besoins et comment construire et
  structurer le nouveau site web
- `Contenu-Site`: Contient le contenu des pages du future site web
- `Wiki`: Structure du future wiki
